import React, { useState } from "react";
import "./App.css";
import ImageGrid from "./components/ImageGrid.";
import ResetComponent from "./components/ResetComponent";
import SettingsComponent from "./components/Settings";
import SettingsContext from "./contexts/SettingsContext";
import { SettingsType } from "./types/Settings.types";

function App() {
  const [settings, setSettings] = useState<SettingsType>({ numOfImages: 20, maxClicks: 5 });
  const [totalClicks, setTotalClicks] = useState(0);

  const incrementTotalClicks = () => {
    setTotalClicks(totalClicks + 1);
  };
  return (
    <div className="App">
      <ResetComponent resetClicksCallback={() => setTotalClicks(0)} />
      <SettingsContext.Provider value={settings}>
        <br />
        <SettingsComponent setSettings={setSettings} />
        <br />
        <ImageGrid totalClicks={totalClicks} onImgClickCallback={incrementTotalClicks} />
      </SettingsContext.Provider>
      <div className="click-counter">Total clicks: {totalClicks}</div>
    </div>
  );
}

export default App;
