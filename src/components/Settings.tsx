import React, { useContext, useState } from "react";
import SettingsContext from "../contexts/SettingsContext";
import { SettingsType } from "../types/Settings.types";

interface MyProps {
  setSettings: (s: SettingsType) => void;
}
const SettingsComponent: React.FC<MyProps> = ({ setSettings }) => {
  const settings = useContext(SettingsContext);

  const onNumofImagesChanged = (numOfImages: number) => {
    setSettings({ ...settings, numOfImages });
  };

  const maxClicksChanged = (maxClicks: number) => {
    setSettings({ ...settings, maxClicks });
  };

  return (
    <div>
      <label>Num of images: </label>
      <input
        type="number"
        placeholder="20"
        min={1}
        value={settings.numOfImages}
        onChange={(e) => onNumofImagesChanged(parseInt(e.currentTarget.value))}
      ></input>
      <br></br>
      <label>Num of clicks allowed</label>
      <input
        type="number"
        placeholder="5"
        value={settings.maxClicks}
        onChange={(e) => maxClicksChanged(parseInt(e.currentTarget.value))}
      ></input>
    </div>
  );
};

export default SettingsComponent;
