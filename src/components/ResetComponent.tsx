import "./ResetComponent.css";

interface MyProps {
  resetClicksCallback: () => void;
}

const ResetComponent: React.FC<MyProps> = ({ resetClicksCallback }) => {
  return (
    <button className="reset-button" onClick={resetClicksCallback}>
      RESET
    </button>
  );
};

export default ResetComponent;
