import { useContext, useEffect, useMemo, useRef, useState } from "react";
import SettingsContext from "../contexts/SettingsContext";
import "./SingleImage.css";

interface MyProps {
  totalClicks: number;
  onImgClickCallback: () => void;
}

const SingleImage: React.FC<MyProps> = ({ totalClicks, onImgClickCallback }) => {
  const [clickCounter, setClickCounter] = useState(0);
  const imgRef = useRef<HTMLImageElement>(null);

  const getRandomNumber = () => {
    return Math.random() * 100000;
  };

  const borderColor = useMemo(
    () => getRandomNumber().toString(16).substring(6, 12),
    [clickCounter]
  );

  useEffect(() => {
    imgRef.current!.src = `https://picsum.photos/200?random=${getRandomNumber()}`;
  }, []);

  useEffect(() => {
    if (totalClicks === 0) {
      setClickCounter(0);
    }
  }, [totalClicks]);

  const settings = useContext(SettingsContext);

  const onImageClick = () => {
    if (clickCounter < settings.maxClicks) {
      imgRef.current!.src = `https://picsum.photos/200?random=${getRandomNumber()}`;
      setClickCounter(clickCounter + 1);
      onImgClickCallback();
    }
  };

  return (
    <div
      className={`single-image ${clickCounter < settings.maxClicks ? "" : "unclickable"}`}
      style={{ borderColor: `#${borderColor}` }}
    >
      <img ref={imgRef} onClick={onImageClick}></img>
      <label className="counter">{clickCounter}</label>
    </div>
  );
};

export default SingleImage;
