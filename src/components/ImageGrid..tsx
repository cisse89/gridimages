import SingleImage from "./SingleImage";
import "./ImageGrid.css";
import { useContext } from "react";
import SettingsContext from "../contexts/SettingsContext";

interface MyProps {
  totalClicks: number;
  onImgClickCallback: () => void;
}

const ImageGrid: React.FC<MyProps> = ({ totalClicks, onImgClickCallback }) => {
  const settingsContext = useContext(SettingsContext);

  const renderImages = (n: number) => {
    if (!n) return <></>;
    return [...Array(n)].map((val, ind) => {
      return (
        <SingleImage key={ind} totalClicks={totalClicks} onImgClickCallback={onImgClickCallback} />
      );
    });
  };

  return <div className="grid">{renderImages(settingsContext.numOfImages)}</div>;
};

export default ImageGrid;
