export type SettingsType = {
  numOfImages: number;
  maxClicks: number;
};
