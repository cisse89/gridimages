import React from "react";
import { SettingsType } from "../types/Settings.types";

export default React.createContext<SettingsType>({ numOfImages: 20, maxClicks: 5 });
